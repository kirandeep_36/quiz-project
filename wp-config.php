<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'quiz_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'S3l25A79S#HLtxg=n<(5;eH7a9v3@cW{$bqvAjd~Tv4{^-T ds:-$Y6x_5qAd?[2' );
define( 'SECURE_AUTH_KEY',  '*Z#LwPgR3`B^uXI3BOy{}K+}$o&!wPHLjsc_L@nu=1j]U yEW_~F522yPfyJbQzl' );
define( 'LOGGED_IN_KEY',    'ial2*tF]tg-TelT:Q^(~oemH{0 ,i]8Q>B9{D[cn6fw(s:,OsZEV U>Q!3?!T}zS' );
define( 'NONCE_KEY',        't*7i84K6sb/e^V;.61:&&7kZB2!  3FDiON ^;*MF).)1L7uRB-?rQ0+]c|Tn+x;' );
define( 'AUTH_SALT',        ',!mup0Gz<~k|9KC.!hH-kz@J)ri6g0)jLrCXBy3qXpGV|c@t74rYvK3T3JL>Sioa' );
define( 'SECURE_AUTH_SALT', 'A!<V]cb(r2.N5!FQ{%29d|%tbFq6wW,MARPHb^R EQ+4?G2pY|6CRc)4_B ^p{@f' );
define( 'LOGGED_IN_SALT',   '<4C8LLKYbzNsHDM*W-W 1,aDd>}Tu|$.a[^7q*g^`<Fbl`Vn;:F4@vSn[r{$=5qe' );
define( 'NONCE_SALT',       'JwPIuP6lofvOc _,`s3,}|CR(ms;o:>T7j~@ci<>PfcxW0G?C{a+9aV3p+I_vC:q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
