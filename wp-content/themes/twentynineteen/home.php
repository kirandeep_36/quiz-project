<?php
/**
 * Template name: Home
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>


	<section id="primary" class="content-area">
		<main id="main" class="site-main">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php if ( ! twentynineteen_can_show_post_thumbnail() ) : ?>
				<header class="entry-header">
					<?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
				</header>
				<?php endif; ?>

				<div class="entry-content">
					<?php
						$post_id;
					if ( have_posts() ) {

						// Load posts loop.
						while ( have_posts() ) {
							the_post();
							//get_template_part( 'template-parts/content/content' );
							$question_content = get_the_content();
							$post_id = get_the_id();
							//echo $question_content;
							global $wpdb;
							$table_name = $wpdb->prefix .'custom_options';
							$user = $wpdb->get_results( "SELECT opt_id FROM $table_name  ORDER BY id DESC LIMIT 1" );

							$last_opt_id = $user[0]->opt_id;

							for ($i=1; $i <= $last_opt_id ; $i++) { 
								  $option_val = replace_with_option($i); 
								$hash_tag = '[#'.$i.']';
								$question_content =  str_replace($hash_tag, $option_val, $question_content);
							}
							
							echo $question_content;
						}

						// Previous/next page navigation.
						twentynineteen_the_posts_navigation();

					} else {

						// If no content, include the "No posts found" template.
						get_template_part( 'template-parts/content/content', 'none' );

					}
					?>
					<input type="hidden" name="selected_option_ids" value="">
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'twentynineteen' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
					</main><!-- .site-main -->
				</section><!-- .content-area -->

<?php
get_footer();
