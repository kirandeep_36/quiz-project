<?php
/**
 * @package Quizs
 */
/*
Plugin Name: ST Quizzes
Plugin URI: https://quizs.com/
Description: This is a good plugin for quiz using options.
Version: 1.1
Author: ST solutions
License: later
Text Domain: Quizzes
*/


// Make sure we don't expose any info if called directly

if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

register_activation_hook( __FILE__, array( 'Quiz', 'plugin_activation' ) );
register_deactivation_hook( __FILE__, array( 'Quiz', 'plugin_deactivation' ) );




/*================ Add Common Files ===============*/
function add_scripts_files() {
  wp_enqueue_style('font-awesome','https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
  wp_enqueue_style('boostrap-css', plugins_url('css/bootstrap.min.css',__FILE__ ));
  wp_enqueue_style('tagsinput.css', plugins_url('css/tagsinput.css',__FILE__ ));
  wp_enqueue_script('popper.min.js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js');
  wp_enqueue_script('bootstrap.min.js', plugins_url('js/bootstrap.min.js',__FILE__ ));
  wp_enqueue_script('tagsinput.js', plugins_url('js/tagsinput.js',__FILE__ ));
  wp_enqueue_script('custom.js', plugins_url('js/custom.js',__FILE__ ));
}
add_action('admin_enqueue_scripts', 'add_scripts_files');


/*==============  Add Files for Front End Pages ============*/
function add_front_end_script_files(){
	wp_enqueue_script('mainJquery', 'https://code.jquery.com/jquery-3.4.1.js');
	wp_enqueue_script('custom.js', plugins_url('js/front/front-custom.js',__FILE__ ));
}
add_action('init', 'add_front_end_script_files');




/*========================== Custom Post Type Roles =================*/
	function my_custom_post_roles() {
		$labels = array(
		'name' => _x( 'Character Roles', 'post type general name' ),
		'singular_name' => _x( 'Role', 'post type singular name' ),
		'add_new' => _x( 'Add New', 'Role' ),
		'add_new_item' => __( 'Add New Role' ),
		'edit_item' => __( 'Edit Role' ),
		'new_item' => __( 'New Role' ),
		'all_items' => __( 'All Roles' ),
		'view_item' => __( 'View Role' ),
		'search_items' => __( 'Search roles' ),
		'not_found' => __( 'No role found' ),
		'not_found_in_trash' => __( 'No role found in the Trash' ),
		'parent_item_colon' => '',
		'menu_name' => 'Character Roles'
		);

		// args array
		$args = array(
		'labels' => $labels,
		'description' => 'Displays roles and their description',
		'public' => true,
		'menu_icon'    => 'dashicons-id',
		'menu_position' => 4,
		'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' /*'custom-fields'*/ ),
		'has_archive' => true,
		);

	register_post_type( 'Role', $args );
	}
	add_action( 'init', 'my_custom_post_roles' ); 




	//================== Add The Quick & Dirty Meta Box ================

		function wpse_add_quick_dirty_metabox() {
		   add_meta_box(
		       'quick_dirty_metabox',       // $id
		       'The Quick & Dirty',        // $title
		       'show_quick_dirty_metabox',  // $callback
		       'role',                 	  // $Post Type/Role
		       'normal',                  // $context
		       'high'                     // $priority
		   );
		}
		add_action('add_meta_boxes', 'wpse_add_quick_dirty_metabox');

		function show_quick_dirty_metabox(){
			global $post;
			$data = get_post_meta($post->ID, 'quick_dirty_metabox', true);

			wp_nonce_field( basename( __FILE__ ), 'wpse_quick_dirty_metabox_nonce' );
			if(!empty($data)){
				echo "<textarea rows='5'style='width:100%' name='quick_dirty_metabox'>$data</textarea>";
			} else{
				echo "<textarea rows='5'style='width:100%' name='quick_dirty_metabox'></textarea>";
			}
		}






	//================== Add The Lust Language Meta Box ================
		function wpse_add_lust_language_metabox() {
		   add_meta_box(
		       'lust_language_metabox',       // $id
		       'Lust Language',        // $title
		       'show_lust_language_metabox',  // $callback
		       'role',                 	  // $Post Type/Role
		       'normal',                  // $context
		       'high'                     // $priority
		   );
		}
		add_action('add_meta_boxes', 'wpse_add_lust_language_metabox');

		function show_lust_language_metabox(){
			global $post;
			$data = get_post_meta($post->ID, 'lust_language_metabox', true);

			wp_nonce_field( basename( __FILE__ ), 'wpse_lust_language_metabox_nonce' );
			if(!empty($data)){
				echo "<textarea rows='5'style='width:100%' name='lust_language_metabox'>$data</textarea>";
			} else{
				echo "<textarea rows='5'style='width:100%' name='lust_language_metabox'></textarea>";
			}
		}




		//================== Add The Going Steady Meta Box ================
			function wpse_add_going_steady_metabox() {
			   add_meta_box(
			       'going_steady_metabox',       // $id
			       'Going Steady',        // $title
			       'show_going_steady_metabox',  // $callback
			       'role',                 	  // $Post Type/Role
			       'normal',                  // $context
			       'high'                     // $priority
			   );
			}
			add_action('add_meta_boxes', 'wpse_add_going_steady_metabox');

			function show_going_steady_metabox(){
				global $post;
				$data = get_post_meta($post->ID, 'going_steady_metabox', true);

				wp_nonce_field( basename( __FILE__ ), 'wpse_going_steady_metabox_nonce' );
				if(!empty($data)){
					echo "<textarea rows='5'style='width:100%' name='going_steady_metabox'>$data</textarea>";
				} else{
					echo "<textarea rows='5'style='width:100%' name='going_steady_metabox'></textarea>";
				}
			}




			//================== Add The Kink Compatibility Meta Box ================
			function wpse_add_kink_compatibility_metabox() {
			   add_meta_box(
			       'kink_compatibility_metabox',       // $id
			       'Kink Compatibility',        // $title
			       'show_kink_compatibility_metabox',  // $callback
			       'role',                 	  // $Post Type/Role
			       'normal',                  // $context
			       'high'                     // $priority
			   );
			}
			add_action('add_meta_boxes', 'wpse_add_kink_compatibility_metabox');

			function show_kink_compatibility_metabox(){
				global $post;
				$data = get_post_meta($post->ID, 'kink_compatibility_metabox', true);

				wp_nonce_field( basename( __FILE__ ), 'wpse_kink_compatibility_metabox_nonce' );
				if(!empty($data)){
					echo "<textarea rows='5'style='width:100%' name='kink_compatibility_metabox'>$data</textarea>";
				} else{
					echo "<textarea rows='5'style='width:100%' name='kink_compatibility_metabox'></textarea>";
				}
			}






	//==================  Add custom option ansers metabox ===================
		function wpse_add_custom_meta_box_2() {
		   add_meta_box(
		       'custom_meta_box-2',       // $id
		       'Question Answers',        // $title
		       'show_custom_meta_box_2',  // $callback
		       'role',                 	  // $Post Type/Role
		       'normal',                  // $context
		       'high'                     // $priority
		   );
		}
		add_action('add_meta_boxes', 'wpse_add_custom_meta_box_2');


		//======== Function to add metabox option as per stored values in table
		function show_custom_meta_box_2(){
			
			global $wpdb, $post;
			$table_name = $wpdb->prefix .'custom_options';
			$table_name_2 = $wpdb->prefix .'role_custom_ans';
			

			$user = $wpdb->get_results( "SELECT opt_id FROM $table_name  ORDER BY id DESC LIMIT 1" );
			$last_opt_id = $user[0]->opt_id;

			
			echo "<table cellpadding='10' cellspacing='10'>";
			echo "<thead><tr><th>Question No.</th><th>Select an answer</th></tr></thead>";
			
			for ($i=1; $i <= $last_opt_id ; $i++) { 
				$option_val = question_table_data($i);
				$opt_data = $wpdb->get_results( "SELECT * FROM $table_name_2  WHERE opt_id = $i and role_id	 = $post->ID" );
				
				echo "<tr><td width='200'><strong>#".$i."</strong></td><td>";
				foreach ($option_val as $key => $value) {
					
					echo "<select class='mdb-select' id='opt_drop_down".$key."' name='opt_box_".$i."'>";
					echo "<option value=''>-----Select an answer-----</option>";

					$str = 'A';
					foreach ($value as $key => $val) {
						if(!empty($opt_data)){

							// Loop to check option with data table
							foreach ($opt_data as $key => $optVal) {
								if($optVal->opt_ans === $str){
									echo "<option value='".$str."' selected>".$val."</option>";
								} else {
									echo "<option value='".$str."'>".$val."</option>";
								}
							}
						} else{
							echo "<option value='".$str."'>".$val."</option>";
						}

						$str++;
					}
				}
				echo "</select></td></tr>"; 
			}
			echo "</table>";
		}








		//======= HOOK TO SAVE OPTION METABOX =============
		add_action( 'save_post', 'my_save_post_function', 10, 3 );

		//======= Function to SAVE option Metabox =========
		function my_save_post_function( $post_ID, $post, $update ) {
			if($post->post_type == 'role'){	

				global $wpdb;
				$table_name = $wpdb->prefix .'custom_options';
				$user = $wpdb->get_results( "SELECT opt_id FROM $table_name  ORDER BY id DESC LIMIT 1" );
				$last_opt_id = $user[0]->opt_id;

				$ans = array();
				
				for ($i=1; $i <= $last_opt_id; $i++) {
					if($_POST['opt_box_'.$i]){
						$ans[$i] = 	$_POST['opt_box_'.$i];
					}
				}

				$table_custom_ans= $wpdb->prefix .'role_custom_ans';
				$wpdb->query("DELETE FROM $table_custom_ans WHERE role_id = $post_ID");

				foreach ($ans as $key => $val) {
					$wpdb->insert($table_custom_ans, array(
						'opt_id'=> $key,
						'role_id'=> $post_ID,
						'opt_ans' => $val
					));
				}


				//Add Custom Metaboxes Values 
				if($_POST['quick_dirty_metabox']){
					update_post_meta($post_ID, 'quick_dirty_metabox',  $_POST['quick_dirty_metabox']);
				}


				//Add Custom Metaboxes Values 
				if($_POST['lust_language_metabox']){
					update_post_meta($post_ID, 'lust_language_metabox',  $_POST['lust_language_metabox']);
				}

				//Add Custom Metaboxes Values 
				if($_POST['going_steady_metabox']){
					update_post_meta($post_ID, 'going_steady_metabox',  $_POST['going_steady_metabox']);
				}

				//Add Custom Metaboxes Values 
				if($_POST['kink_compatibility_metabox']){
					update_post_meta($post_ID, 'kink_compatibility_metabox',  $_POST['kink_compatibility_metabox']);
				}





			}// End main If
		} //========= End of Metabox Section =================

/*========================== End of Custom Post Type Roles =================*/









//============ Create Submenu Main qusetion ================
add_action('admin_menu', 'st_quiz_plugin_create_menu');

function st_quiz_plugin_create_menu() {
	add_submenu_page(
	    'edit.php?post_type=role',
	    __( 'Main Question', 'question' ),
	    __( 'Main Question', 'question' ),
	    'manage_options',
	    'question',
	    'st_quiz_plugin_settings_page'
	);

  //Hook to ADD Main Question SubMenu 
  add_action( 'admin_init', 'register_st_quiz_settings' );
}

function register_st_quiz_settings() {
  //register our settings
  register_setting( 'st_quiz-settings-group', 'main_question' );
}


//get values from table
function get_option_val($tab_name){
	global $wpdb;
	$table_name = $wpdb->prefix .$tab_name;
	$user = $wpdb->get_results( "SELECT * FROM $table_name" );
	return $user;
}


function st_quiz_plugin_settings_page() {
  ?>
  <div class="wrap">

  	<!-- ============ Question Form ============= -->
	    <h1>Write your question in below box.</h1>

	    <form method="post" action="options.php">
	    <?php 
	    	settings_fields( 'st_quiz-settings-group' );
		    do_settings_sections( 'st_quiz-settings-group' ); 
		    $question_val = get_option('main_question');
			if ( isset( $_GET['settings-updated'] ) ) {
				echo "<div class='updated'><p>Your question updated successfully.</p></div>";
			} ?>

	      	<div class="form-group">
	      		<textarea name="main_question" style="font-size: 24px; width: 60%" class="form-control" rows="10" required><?php echo $question_val; ?></textarea>
	      	</div>
	      	<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save"></p>
	    </form>
    <!-- ============ End of Question Form ============= -->



    <!-- ============ Question Answers Option Form ============= -->
	    <h1>Add Question's  Option</h1>
	    <form method="post">
	    	<table width="50%" bgcolor="#f2f2f2" cellpadding="10" cellspacing="10">
	    		<tbody>
	    			<?php
						//get option values
						$dataFields = array();
						$option_val = get_option_val("custom_options");
						foreach ($option_val as $key => $value) {
	 						$dataFields[$value->opt_id][]=  $value->option_name;
			    		}
						foreach($dataFields as $key => $val){ 
						?>

						<tr id="<?php echo $key; ?>">
		    				<td width="30%"><strong>#<?php echo $key; ?></strong><td>
		    				<td width="70%">
							<input type="text" name="question_<?php echo $key; ?>_options" data-role="tagsinput" value="<?php foreach($val as $opt_val){ echo $opt_val.','; } ?>">
		    				<td>
		    			</tr>
						<?php }
						
	    			?>
	    		</tbody>
	    	</table>
	    	<div class="form-group">
				<button id="add_new_opt" class="btn btn-default">Add New</button>
			</div>
	    	<p class="submit"><input type="submit" name="submit_opts" id="submit" class="button button-primary" value="Save"></p>
	    </form>
    <!-- ============ End of Options Form ============= -->
  </div>
<?php }


if ( isset( $_POST['submit_opts'] ) ){
	$wpdb->query('TRUNCATE TABLE wp_custom_options');
	unset($_POST['submit_opts']);
	$num = 1;
	foreach ($_POST as $key => $value) {
		$options = explode(',', $value);

		foreach ($options as $val) {
			$data = $wpdb->insert('wp_custom_options', array(
				'opt_id'=> $num,
				'option_name' => $val
			));
		}
		$num++;
	}

}





//========================= Function replace Question[#] with option Dropdown ====================
function replace_with_option($option_id){
	global $wpdb;
	$table_name = $wpdb->prefix .'custom_options';
	$user = $wpdb->get_results( "SELECT opt_id FROM $table_name  ORDER BY id DESC LIMIT 1" );
	$last_opt_id = $user[0]->opt_id;
	$dataFields = array();
	$drop_down_html;
	$user_data = $wpdb->get_results( "SELECT opt_id, option_name FROM $table_name WHERE opt_id = $option_id" );
	
	foreach ($user_data as $key => $value) {
		$dataFields[$value->opt_id][]=  $value->option_name;
	}
	$str = 'A';
	foreach($dataFields as $data_key => $val){ 
			$drop_down_html .= "<span class='option_custon_group'><select class='mdb-select md-form' data-opt_id=".$data_key." name='opt_box_".$data_key."'>";
			$drop_down_html .= "<option value='' selected></option>";
			
			foreach ($val as $key => $opt_val) {
				$drop_down_html .= "<option value='".$str."'>".$opt_val."</option>";
				$str++;
			}
		$drop_down_html .= "</select>";
		$drop_down_html .= "<input type='hidden' name='selected_role_ids[]' class='selected_opt_$data_key'></span>";
	}

	return  $drop_down_html;
}


//Return question table data
function question_table_data($option_id){
	global $wpdb;
	$table_name = $wpdb->prefix .'custom_options';
	$user = $wpdb->get_results( "SELECT opt_id FROM $table_name  ORDER BY id DESC LIMIT 1" );
	$last_opt_id = $user[0]->opt_id;
	$dataFields = array();
	$drop_down_html;
	$user_data = $wpdb->get_results( "SELECT opt_id, option_name FROM $table_name WHERE opt_id = $option_id" );
	
	foreach ($user_data as $key => $value) {
		$dataFields[$value->opt_id][]=  $value->option_name;
	}

	return  $dataFields;
}









/*============= Question Shortcode ==============*/
function front_end_question_shortcode(){	

	$question_content = get_option('main_question');
	$post_id = get_the_id();
	//echo $question_content;
	global $wpdb;
	$table_name = $wpdb->prefix .'custom_options';
	$user = $wpdb->get_results( "SELECT opt_id FROM $table_name  ORDER BY id DESC LIMIT 1" );

	$last_opt_id = $user[0]->opt_id;

	for ($i=1; $i <= $last_opt_id ; $i++) { 
		$option_val = replace_with_option($i); 
		$hash_tag = '[#'.$i.']';
		$question_content =  str_replace($hash_tag, $option_val, $question_content);
	}
		
	$html .= "<div id='short_code_section' class='short_code_div col-sm-12'>
				<input hidden id='lastQust_id' value='$last_opt_id'>
				<form>
				$question_content
				<div class='form-group'>
					<input type='text' class='form-control' name='guest_name' placeholder='Name' required>
				</div>
				<div class='form-group'>
					<input type='email' class='form-control' name='guest_email' placeholder='Email' required>
				</div>

				<div class='form-group'>
					<input type='submit' id='submit_ans_data' class='btn btn-default' value='submit'>
				</div>

			  </form></div>
			  <div class='ajax_result_sect'>
			  	<div class='role_image'></div>
			  </div>
			  ";

	return  $html;

}
add_shortcode('added_question','front_end_question_shortcode');






//====================== Ajax Function to search option Role ID ========================
function ajax_call_back_search(){
	global $wpdb;
	$opt_id = $_GET['optID'];
	$opt_ans = $_GET['opt_val'];

	$table_name = $wpdb->prefix .'role_custom_ans';
	$role_ID = $wpdb->get_row( "SELECT role_id, opt_id FROM $table_name WHERE opt_id = $opt_id AND opt_ans = '$opt_ans'"  );

	if(!empty($role_ID)){
		echo  json_encode($role_ID);
	} else {
		$role_ID = array (
			'role_id' => '',
			'opt_id'  => $opt_id
		);
		echo  json_encode($role_ID);
	}
	wp_die();
}

add_action("wp_ajax_nopriv_ajax_call_back_search", "ajax_call_back_search");
add_action("wp_ajax_ajax_call_back_search", "ajax_call_back_search");





//====================== Ajax Function to get role data on the question's answer behalf ========================
function ajax_get_role_data(){
	
	$role_ids = $_GET['role_ids'];
	$id_counts = array_count_values($role_ids);
	$maxs = array_keys($id_counts, max($id_counts));

	$data['data'] = array();

	//Role ID
	$role_id = $maxs[0];

	//Get Role Data By Role ID
	$data['meta_data'] 	= 	get_post($role_id);
	$data['thumnail'] 	= 	get_the_post_thumbnail_url($role_id);

	echo json_encode($data);

	wp_die();
}

add_action("wp_ajax_nopriv_ajax_get_role_data", "ajax_get_role_data"); 
add_action("wp_ajax_ajax_get_role_data", "ajax_get_role_data");

