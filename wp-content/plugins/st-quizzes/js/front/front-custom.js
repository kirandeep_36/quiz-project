jQuery(document).ready(function(){
	var ajax_url = "http://localhost/quiz-website/wp-admin/admin-ajax.php";

	//========== Function on select option at front page question =====
	jQuery('.mdb-select.md-form').on('change', function(){
		var opt_id = jQuery(this).data('opt_id');
		var selected_opt_val = jQuery(this).children("option:selected").val();
		
		 jQuery.ajax({
              url:     ajax_url,
              contentType: "application/json",
              data:    ({ optID: opt_id, opt_val: selected_opt_val, action  : 'ajax_call_back_search'}),
              success: function(data){
              	var result = JSON.parse(data);
              	jQuery('.selected_opt_'+result.opt_id).val(result.role_id);
              }
        });

	});


	//===========  Questions Answers Data Collect Form Submit Function  ============== 
	jQuery('#submit_ans_data').on('click', function(){
		event.preventDefault();
		var lastQust_id = jQuery('#lastQust_id').val(); 
		var opt_id = jQuery(this).data('opt_id');
		var selected_opt_val = jQuery(this).children("option:selected").val();
			
		var ans_ids = [];
		var temp_val ='';

		for (i = 1; i <= lastQust_id; i++) { 
			console.log(i);
			temp_val = jQuery('.selected_opt_'+i).val();
			// if(!empty(temp)){
			// 	ans_ids.push(temp);
			// }
			if(temp_val != ''){
				ans_ids.push(temp_val);
			}
		}

		 jQuery.ajax({
              url:     ajax_url,
              contentType: "application/json",
              data:    ({role_ids: ans_ids, action : 'ajax_get_role_data'}),
              success: function(data){
              	var result = JSON.parse(data);
              		if(result.meta_data != null){
	              		var role_result = "<p><label class='role_name'>"+result.meta_data.post_title+"</lebel></p><img src='"+result.thumnail+"' alt='role-img'>";
						jQuery('.role_image').append(role_result);
	              		jQuery('#submit_ans_data').remove();
              		}
					//jQuery('.mdb-select.md-form').attr('disabled', true);
					//console.log(data.thumnail);
              	console.log(result.thumnail);
              	//jQuery('.selected_opt_'+result.opt_id).val(result.role_id);
              }
        });

	});
});
