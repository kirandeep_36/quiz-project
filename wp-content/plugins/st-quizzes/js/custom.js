 
 jQuery(document).ready(function(){

	//================ Add New Option Input ===========
	var btn_delete = '<i class="fa fa-trash" id="del_option" data-toggle="tooltip" title="Delete option row" aria-hidden="true"></i>';

	jQuery('#add_new_opt').click(function(){
		var last_qust_no = jQuery('table tr:last').attr('id');
		event.preventDefault();
		var new_no =	parseInt(last_qust_no)+1;
		// alert('#'+new_no+' input');
		jQuery('#'+new_no+' input').tagsinput();
		var add_new_option = '<tr id="'+new_no+'"><td width="30%"><strong>#'+new_no+'</strong><td><td width="70%"><input type="text" name="question_'+new_no+'_options" data-role="tagsinput" value="" required="true"><td></tr>';
		jQuery('#'+last_qust_no).after(add_new_option);
		jQuery('#'+new_no+' input').tagsinput();
		var del_length = jQuery('#del_option').length;
		if(del_length > 0){
			jQuery('table tr#'+last_qust_no+' #del_option').remove();
		}
		jQuery('table tr:last').append(btn_delete);

	});

		var last_qust_no = jQuery('table tr:last').attr('id');
		jQuery(this).closest('tr').remove();
		if(last_qust_no != '1'){
			jQuery('table tr:last').append(btn_delete);
		}


		//Delete Option Row Function
		jQuery('body').on('click','#del_option', function(){
			jQuery(this).closest('tr').remove();
			var del_length = jQuery('#del_option').length;
			var last_qust_no = jQuery('table tr:last').attr('id');
			if(last_qust_no != '1' && del_length < 1){
				jQuery('table tr#'+last_qust_no).append(btn_delete);
			}
		});

});
